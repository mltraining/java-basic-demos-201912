public class BranchingEx {
    public static void main(String[] args) {

//        String city = "Berlin";
        // See siin on demo harjutus!
        /*
            See
            siin
            on
            kommentaar
         */
        String city = args[0];
        if (city.equals("Milano")) {
            System.out.println("The weather is warm!");
        } else {
            System.out.println("The weather is not important anyway!");
        }

        String grade = args[1];
        int gradeNumber = Integer.parseInt(grade);

        System.out.println("If statement example");
        if (gradeNumber > 4) {
            System.out.println("very good");
        } else if (gradeNumber == 4) {
            System.out.println("good");
        } else if (gradeNumber == 3) {
            System.out.println("passable");
        } else if (gradeNumber == 2) {
            System.out.println("not passable");
        } else {
            System.out.println("failed");
        }

        System.out.println("Switch statement sample");
        switch (gradeNumber) {
            case 5:
                System.out.println("very good");
                break;
            case 4:
                System.out.println("good");
                break;
            case 3:
                System.out.println("passable");
                break;
            case 2:
                System.out.println("not passable");
                break;
            case 1:
                System.out.println("failed");
                break;
            default:
                System.out.println("incorrect grade");
        }

        int age = Integer.parseInt(args[2]);

        String ageText = age > 100 ? "old" : "young";
        System.out.println(ageText);

        ageText = (age > 100) ? ("old") : (age == 100 ? "almost old" : "young");
        System.out.println(ageText);

        if (age > 100) {
            ageText = "old";
        } else if (age == 100) {
            ageText = "almost old";
        } else {
            ageText = "young";
        }
        System.out.println(ageText);
    }
}
