import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExceptionHandling {
    public static void main(String[] args) {
        try {
            System.out.println(4 / 1);
            if (args.length > 0) {
                Files.readAllLines(Paths.get(args[0]));
            }
        } catch (IOException e) {
            System.out.println("Did not find the sepcified file.");
        } catch (ArithmeticException e) {
            System.out.println("Please bear in mind, that it is not allowed to divide by zero!");
        } catch (Exception e) {
            System.out.println("Somewhere something went wrong!");
        } finally {
            System.out.println("Anyway, thanks for using our software!");
        }
    }
}
