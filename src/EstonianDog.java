public class EstonianDog extends Dog {

    public EstonianDog(String name) {
        super(name);
    }

    @Override
    public void bark() {
        System.out.println("auh-auh");
    }

}
