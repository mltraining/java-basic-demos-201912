public class Dog {
    private String name;

    public String getName() {
        if (name.equals("Bella")) {
            return "Muki";
        }
        return name;
    }

    public void setName(String name) {
        if (name != null && name.length() > 5) {
            this.name = name;
        }
    }

    public Dog(String name) {
        this.name = name;
    }

    public void bark() {
        System.out.println("Abstract barking...");
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                '}';
    }
}
