public class ItalianDog extends Dog {

    public ItalianDog(String name) {
        super(name); // Baasklassi kontruktori väljakutsumine
    }

    @Override
    public void bark() {
        System.out.println("bau-bau");
    }
}
