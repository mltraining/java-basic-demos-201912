public class ArrayEx {
    public static void main(String[] args) {

        // Exercise 7
        int[] myNumbers;
        myNumbers = new int[5];
        myNumbers[0] = 1;
        myNumbers[1] = 2;
        myNumbers[2] = 3;
        myNumbers[3] = 4;
        myNumbers[4] = 5;
        System.out.println(myNumbers[0]);
        System.out.println(myNumbers[2]);
        System.out.println(myNumbers[myNumbers.length - 1]);

        // Exercise 8
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        System.out.println(cities[0]);
        System.out.println(cities[1]);
        System.out.println(cities[2]);
        System.out.println(cities[3]);

        // Exercise 9
        int[][] numbersArr = new int[3][];
        numbersArr[0] = new int[]{1, 2, 3};
        numbersArr[1] = new int[]{4, 5, 6};
        numbersArr[2] = new int[]{7, 8, 9, 0};
        System.out.println(numbersArr[2][1]);

        // Exercise 10

        // Variant 1
        String[][] countryCities = new String[3][];

        // Eesti
        countryCities[0] = new String[4];
        countryCities[0][0] = "Tallinn";
        countryCities[0][1] = "Tartu";
        countryCities[0][2] = "Valga";
        countryCities[0][3] = "Võru";

        // Rootsi
        countryCities[1] = new String[]{"Stockholm", "Uppsala", "Lund", "Köping"};

        // Soome
        countryCities[2] = new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"};


        // Variant 2
        String[][] countryCities2 = {
                {"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"}
        };

        System.out.println(countryCities2[2][3]);

        for(String[] currentCountryCities : countryCities2) {
            for(String city : currentCountryCities) {
                System.out.println(city);
            }
        }

    }
}
