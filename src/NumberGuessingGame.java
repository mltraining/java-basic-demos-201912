import java.util.*;

public class NumberGuessingGame {

    private static Scanner scanner = new Scanner(System.in);
    private static List<Result> results = new ArrayList<>();

    public static void main(String[] args) {

        while(true) {
            System.out.println("Palun sisesta oma nimi:");
            String userName = getTextFromConsole();
            int guessCount = play();
            results.add(new Result(userName, guessCount));

            System.out.println("Mida soovid teha? 1 - uus mäng, 2 - välju mängust");
            int userInput = getNumberFromConsole();

            if (userInput == 2) {
                break;
            }
        }

        displayBoard();
        System.out.println("Nägemist!");
        System.out.println("Copyright: all rights reserved! Marek Lints");
    }

    private static void displayBoard() {
        Collections.sort(results, (result1, result2) -> {
            // kui tagastad < 0, siis elemendid on õiget pidi
            // kui tagastad > 0, siis elemendid on valet pidi
            // kui tagastad 0, on elemendid võrdsed
//            if (result1.count > result2.count) {
//                return -1;
//            } else {
//                return 1;
//            }
            return result1.count - result2.count;
        });
        System.out.println("--------------------------");
        System.out.println("EDETABEL");
        System.out.println("--------------------------");
        for(Result currentResult : results) {
            System.out.print("Nimi: " + currentResult.getName());
            System.out.println(" Kordi: " + currentResult.getCount());
            System.out.println("--------------------------");
        }
    }

    private static int play() {
        int randomNumber = (int) (Math.random() * 3 + 1);

        System.out.println("Arva ära number vahemikus 1 - 10 000!");

        int userNumber = 0;
        int guessCount = 0;
        do {
            guessCount++;
            System.out.println("Ütle number:");
            userNumber = getNumberFromConsole();

            if (userNumber == randomNumber) {
                System.out.println("Õige, sul kulus " + guessCount + " korda, et ära arvarta!");
            } else if (userNumber > randomNumber) {
                System.out.println("Liiga suur!");
            } else {
                System.out.println("Liiga väike!");
            }
        } while (userNumber != randomNumber);
        return guessCount;
    }

    private static int getNumberFromConsole() {
        while (true) {
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Ebakorrektene number.");
            } finally {
                scanner.nextLine(); // Workaround for a bug.
            }
        }
    }

    private static String getTextFromConsole(){
        return scanner.nextLine();
    }

    public static class Result {
        private String name;
        private int count;

        public Result(String name, int count) {
            this.name = name;
            this.count = count;
        }

        public String getName() {
            return name;
        }

        public int getCount() {
            return count;
        }

        @Override
        public String toString() {
            return "Result{" +
                    "name='" + name + '\'' +
                    ", count=" + count +
                    '}';
        }
    }
}
