
import java.util.*;

public class CollectionsEx {
    public static void main(String[] args) {
        System.out.println("Exercise 20");
        List<String> citiesOfWorld = new ArrayList<>();
        citiesOfWorld.add("Barcelona");
        citiesOfWorld.add("Birmingham");
        citiesOfWorld.add("Baden-Baden");
        citiesOfWorld.add("New York");
        citiesOfWorld.add("Kohila");
//        List<String> citiesOfWorld = Arrays.asList("Barcelona", "Birmingham", "Baden-Baden", "New York", "Kohila");
        System.out.println(citiesOfWorld.get(0));
        System.out.println(citiesOfWorld.get(2));
        System.out.println(citiesOfWorld.get(citiesOfWorld.size() - 1));

        System.out.println("Exercise 21");
        Set<String> personNames = new HashSet<>();
        personNames.add("Kati");
        personNames.add("Mati");
        personNames.add("Mare");
        personNames.add("Malle");
        personNames.add("Mati");
        System.out.println(personNames.size());

        personNames.forEach(System.out::println);

        for (String name : personNames) {
            System.out.println(name);
        }

        System.out.println("Exercise 22");
        Map<String, String[]> countryCities = new HashMap<>();
        countryCities.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        countryCities.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        countryCities.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

        for (String countryName : countryCities.keySet()) {
            System.out.println("Country: " + countryName);
            System.out.println("Cities:");
            for(String city : countryCities.get(countryName)) {
                System.out.println("\t\t\t\t\t\t" + city);
            }
        }
    }
}
