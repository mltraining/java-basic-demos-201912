import javafx.print.Collation;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class LoopsEx {
    public static void main(String[] args) {

        System.out.println("Exercise 11");
        int number = 1;
        while (number < 101) {
            System.out.println(number);
            number = number + 1;
        }

        System.out.println("Exercise 12");
        for (int i = 1; i < 101; i++) {
            System.out.println(i);
        }

        System.out.println("Exercise 13");
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int thisNumber : numbers) {
            System.out.println(thisNumber);
        }

        System.out.println("Exercise 14");
        for (int i = 1; i < 101; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

        System.out.println("Exercise 15");
        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
        String commaSeparatedBands = "";

        for (int i = 0; i < bands.length; i++) {
            commaSeparatedBands = commaSeparatedBands + bands[i];
            if (i < bands.length - 1) {
                commaSeparatedBands = commaSeparatedBands + ", ";
            }
        }

//        commaSeparatedBands = String.join(", ", bands);

        System.out.println(commaSeparatedBands);

        System.out.println("Exercise 16");
        commaSeparatedBands = "";

        for (int i = bands.length - 1; i >= 0; i--) {
            commaSeparatedBands = commaSeparatedBands + bands[i];
            if (i > 0) {
                commaSeparatedBands = commaSeparatedBands + ", ";
            }
        }
        System.out.println(commaSeparatedBands);

        List<String> bandsReverse = Arrays.asList(bands);
        Collections.reverse(bandsReverse);
        System.out.println(String.join(", ", bandsReverse.toArray(String[]::new)));

        System.out.println("Exercise 19");
        for(int i = 0; i < 8; i++) {
            for (int j = 0; j < 19; j++) {
                if(j % 2 == 0) {
                    if(i % 2 == 0) {
                        System.out.print("#");
                    } else {
                        System.out.print("+");
                    }
                } else {
                    if(i % 2 == 0) {
                        System.out.print("+");
                    } else {
                        System.out.print("#");
                    }
                }
            }
            System.out.println();
        }
    }
}
