public class Methods {
    public static void main(String[] args) {

        int myLargeNumber = tripleNumber(5);
        System.out.println(myLargeNumber);
    }

    private static int tripleNumber(int number) {
        int result = 3 * number;
        return result;
    }
}
