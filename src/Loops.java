public class Loops {
    public static void main(String[] args) {

        int[] iArr = {4, 1, 9, 3};
        int currentIndex = 0;
        System.out.println("While loop");
        while (currentIndex < iArr.length) {
            System.out.println(iArr[currentIndex]);
            currentIndex++;
        }

        currentIndex = 0;
        System.out.println("Do-while loop");
        do {
            System.out.println(iArr[currentIndex]);
            currentIndex++;
        } while (currentIndex < iArr.length);

        System.out.println("For loop");
        for(int i = 0; i < iArr.length; i++) {
            System.out.println(iArr[i]);
        }

        System.out.println("Foreach loop");
        for(int tmpNumber : iArr) {
            System.out.println(tmpNumber);
        }
    }
}
