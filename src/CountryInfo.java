import java.util.Arrays;

public class CountryInfo { // extends Object
    public String countryName;
    public String[] cities;
    public int population;

    public CountryInfo() {

    }

    public CountryInfo(String countryName, String[] cities, int population) {
        this.countryName = countryName;
        this.cities = cities;
        this.population = population;
    }

    @Override
    public String toString() {
        String info = "Country: " + this.countryName + "\n";
        info = info + "Population: " + this.population + "\n";
        info = info + "Cities:\n";
        for(String city : cities) {
            info = info + "\t" + city + "\n";
        }

        return info;
    }
}
