public class BankAccount {
    public String iban;
    public double balance;

    // Behaviour
    public boolean deposit(double amount) {
        this.balance = this.balance + amount;
        return false;
    }

    public boolean withdraw(double amount) {
        this.balance = this.balance - amount;
        return false;
    }

    public boolean transfer(double amount, String targetIban) {
        return false;
    }
}
