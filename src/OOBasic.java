public class OOBasic {
    public static void main(String[] args) {
        BankAccount iban1 = new BankAccount();
        iban1.balance = 10000;
        iban1.iban = "EE123";

        BankAccount iban2 = new BankAccount();
        iban2.balance = 56;
        iban2.iban = "SE567";

        System.out.println(iban1.iban + ": ");
        System.out.println(iban1.balance);
        iban1.deposit(45.0);

        System.out.println(iban2.iban + ": ");
        System.out.println(iban2.balance);
        iban2.withdraw(4);


    }
}
