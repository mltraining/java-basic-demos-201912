public class MethodEx {
    public static void main(String[] args) {

        System.out.println("Exercise 1");
        System.out.println(test(456));
        boolean myResult = test(3478);
        System.out.println(myResult);
        test(45645);

        System.out.println("Exercise 2");
        test2("Tere", "Maailm");
        test2(null, null);

        System.out.println("Exercise 3");
        System.out.println("Toote hind käibemaksuta: " + 100.0);
        System.out.println("Toote hind käibemaksuga: " + addVat(100.0));
        System.out.println("Toote hind käibemaksuta: " + 67.89);
        System.out.println("Toote hind käibemaksuga: " + addVat(67.89));
        System.out.println("Inimene isikukoodiga 49403136526 on " +
                extractPersonGender("49403136526"));
        System.out.println("Inimene isikukoodiga 49403136526 on sündinud aastal " +
                extractPersonBirthYear("49403136526"));
        System.out.println("Kas inimese isikukood 49403136526 on korrektne? " +
                isPersonalCodeValid("49403136526"));
    }

    private static boolean test(int inputParam) {
        return true;
    }

    public static void test2(String p1, String p2) {

    }

    public static double addVat(double price) {
        return 1.2 * price;
    }

    public static String extractPersonGender(String personalCode) { // personalCode = "49403136526"
//        System.out.println(Integer.parseInt("" + personalCode.charAt(0)));
//        Integer.parseInt(personalCode.substring(0, 1)); // "49403136526" --> "4"

        // Variant 1:
//        String genderDigit = personalCode.substring(0, 1);
//        if (genderDigit.equals("1") || genderDigit.equals("3") ||
//                genderDigit.equals("5") || genderDigit.equals("7")) {
//            return "M";
//        } else {
//            return "F";
//        }

        // Variant 2:
        int genderDigit = Integer.parseInt(personalCode.substring(0, 1));
        return genderDigit % 2 == 0 ? "F" : "M";
    }

    public static int extractPersonBirthYear(String personalCode) { // personalCode = "49403136526"
        int genderDigit = Integer.parseInt(personalCode.substring(0, 1)); // 4
        int birthYearDigits = Integer.parseInt(personalCode.substring(1, 3)); // 94

        switch (genderDigit) {
            case 1:
            case 2:
                return 1800 + birthYearDigits;
            case 3:
            case 4:
                return 1900 + birthYearDigits;
            case 5:
            case 6:
                return 2000 + birthYearDigits;
            case 7:
            case 8:
                return 2100 + birthYearDigits;
            default:
                return -1;
        }
    }

    public static boolean isPersonalCodeValid(String personalCode) {
        String[] personalCodeDigits = personalCode.split("");
        int[] personCodeNumbers = new int[11];
        for (int i = 0;  i < 11; i++) {
            personCodeNumbers[i] = Integer.parseInt(personalCodeDigits[i]);
        }

        int[] weights = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        int sum = 0;

        for(int i = 0; i < weights.length; i++) {
            sum = sum + weights[i] * personCodeNumbers[i];
        }

        int checkNumber = sum % 11;

        if (checkNumber != 10) { // Happy case
            return checkNumber == personCodeNumbers[10];
        } else {
            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            sum = 0;
            for(int i = 0; i < weights2.length; i++) {
                sum = sum + weights2[i] * personCodeNumbers[i];
            }

            checkNumber = sum % 11;

            if (checkNumber != 10) {
                return checkNumber == personCodeNumbers[10];
            } else {
                return personCodeNumbers[10] == 0;
            }
        }
    }
}
