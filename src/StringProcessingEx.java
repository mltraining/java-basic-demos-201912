public class StringProcessingEx {
    public static void main(String[] args) {

        String s1 = "Hello, World!";
        System.out.println(s1);

        System.out.println("Hello, \"World\"!");

        String stevenHawkingQuote = "Steven Hawking once said: " +
                "\"Life would be tragic, if it weren\'t funny\".";
        System.out.println(stevenHawkingQuote);

        String tallinnPopulation = "450 000";
        String tallinnSizeText = "Tallinnas elab " + tallinnPopulation + " inimest.";
        System.out.println(tallinnSizeText);

        int populationOfTallinn = 450_000;
        String tallinnSizeText2 = "Tallinnas elab " + populationOfTallinn + " inimest.";
        System.out.println(tallinnSizeText2);

        String tallinnSizeText3 = String.format("Tallinnas elab %,d inimest.", populationOfTallinn);
        System.out.println(tallinnSizeText3);

        String planet1 = "Merkuur";
        String planet2 = "Venus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uran";
        String planet8 = "Neptuun";
        int planetCount = 8;

        String planetsText = String.format("%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %d planeeti.",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount);
        System.out.println(planetsText);

        planetsText = planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " +
                planet5 + ", " + planet6 + ", " + planet7 + " ja " + planet8 + " on Päikesesüsteemi "
                + planetCount + " planeeti.";
    }
}
