public class HelloWorld {
    public static void main(String[] xxx) {

        if (xxx.length < 4) {
            System.out.println("Liiga vähe parameetreid!");
        }

        System.out.println("Hello, World!!!");
        System.out.println("Käsurea parameeter: " + xxx[0]);

        int myNumber = 5;
        System.out.println(myNumber);
        myNumber = 6;
        System.out.println(myNumber);

        byte mySmallNumber = (byte) 556;
        System.out.println(mySmallNumber);

        short myMediumSizedNumber = 556;
        System.out.println(myMediumSizedNumber);

        boolean isItTrue = true;
        System.out.println(isItTrue);

        double myDecimalNumber = 3.14;
        System.out.println(myDecimalNumber);

        double d1 = 5.6;
        double d2 = 5.8;
        double d3 = d1 + d2;
        System.out.println(d3);

        float myFloat = 6.7f;

        long myLargeNumber = 56000000000L;

        char myChar = 'A';
        System.out.println((byte)myChar);
        char myChar2 = 'B';
        System.out.println((byte)myChar2);
        byte mySmallNumber2 = 78;
        System.out.println((char)mySmallNumber2);

    }
}
