import java.util.ArrayList;
import java.util.List;

public class OoEx {
    public static void main(String[] args) {
        System.out.println("Exercise 1");
        CountryInfo country1 = new CountryInfo("Estonia",
                new String[]{"Tallinn", "Tartu", "Valga", "Võru"}, 1300000);

        CountryInfo country2 = new CountryInfo();
        country2.countryName = "Sweden";
        country2.cities = new String[]{"Stockholm", "Uppsala", "Lund", "Köping"};
        country2.population = 10000000;

        CountryInfo country3 = new CountryInfo();
        country3.countryName = "Finland";
        country3.cities = new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"};
        country3.population = 5500000;

        List<CountryInfo> countries = new ArrayList<>();
        countries.add(country1);
        countries.add(country2);
        countries.add(country3);

        for (CountryInfo currentCountry : countries) {
            System.out.println(currentCountry);
//            System.out.println("---");
//            System.out.println(currentCountry.countryName);
//            System.out.println(currentCountry.population);
//            for (String city : currentCountry.cities) {
//                System.out.println("\t" + city);
//            }
        }

        Dog generalDog = new Dog("Generico");
        generalDog.setName("Super Generico");
        generalDog.bark();

        Dog myItalianDog = new ItalianDog("Bella");
        myItalianDog.setName("Super Bella");
        System.out.println(myItalianDog);
        myItalianDog.bark();

        Dog myJapaneseDog = new JapaneseDog("Haruto");
        System.out.println(myJapaneseDog);
        myJapaneseDog.bark();

        EstonianDog myEstonianDog = new EstonianDog("Muki");
        System.out.println(myEstonianDog);
        myEstonianDog.bark();
    }
}
